//Buttons value init
int restart = 0;
int button3 = 0;
int button4 = 0;
int button5 = 0;
int button6 = 0;

//Other Global vars
int nLeds = 4;

//void simonSays();
//bool simonChallenge(int difficulty);

void setup() {
  // put your setup code here, to run once:
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, INPUT);
  pinMode(5, INPUT);
  pinMode(6, INPUT);
  
  Serial.begin(9600);      // open the serial port at 9600 bps:    
}

void loop() {
  // put your main code here, to run repeatedly:
  restart = digitalRead(2);

  if (restart == HIGH) {
    //Start game
    simonSays();
    digitalWrite(7, LOW);
    digitalWrite(8, LOW);
    digitalWrite(9, LOW);
    digitalWrite(10, LOW);
    //CONTROL LED
    digitalWrite(11, LOW);
  } 
}


void simonSays() {
  int i = 0;
  bool failure = false;
  while (!failure) {
    i++;
    //Difficulty blink
    for(int j = 0; j < i; j++){
      digitalWrite(11, HIGH);
      delay(200);
      digitalWrite(11, LOW);
      delay(100);
    }
    failure = simonChallenge(i);
  }
  //Fin de la ejecución
  digitalWrite(7, HIGH);
  digitalWrite(8, HIGH);
  digitalWrite(9, HIGH);
  digitalWrite(10, HIGH);
  digitalWrite(11, HIGH);
  delay(50);
  digitalWrite(7, LOW);
  digitalWrite(8, LOW);
  digitalWrite(9, LOW);
  digitalWrite(10, LOW);
  digitalWrite(11, HIGH);

  delay(250);
}

bool simonChallenge(int difficulty){
  int sequence[difficulty];
  int solutions[difficulty];
  int hits = 0;
  
  //The random sequence is created
  for (int i = 0; i < difficulty; i++) {

    //Random LED is chosen
    sequence[i] = random(7, 11);
    Serial.println(sequence[i]);
    //Blink of the new LED inserted
    digitalWrite(sequence[i], HIGH);
    delay(250);
    digitalWrite(sequence[i], LOW);
    delay(250);
    //Each LED is associated with its button    
    solutions[i] = sequence[i] - nLeds;
  }
  //Check user input
  while (hits < difficulty) {
    button3 = digitalRead(3);
    button4 = digitalRead(4);
    button5 = digitalRead(5);
    button6 = digitalRead(6);
    
    //Button3 validation
    if (button3 == HIGH) {
      int ans = 3;
      if (solutions[hits] == ans) {
        digitalWrite((ans+nLeds), HIGH);
        delay(350);
        digitalWrite((ans+nLeds), LOW);
        delay(200);
      }
      else {
        return true;
      } 
      hits++;
    }

    
    //Button4 validation
    if (button4 == HIGH) {
      int ans = 4;
      if (solutions[hits] == ans) {
        digitalWrite((ans+nLeds), HIGH);
        delay(350);
        digitalWrite((ans+nLeds), LOW);
        delay(200);
      }
      else {
        return true;
      } 
      hits++;
    }
    //Button5 validation
    if (button5 == HIGH) {
      int ans = 5;
      if (solutions[hits] == ans) {
        digitalWrite((ans+nLeds), HIGH);
        delay(350);
        digitalWrite((ans+nLeds), LOW);
        delay(200);
      }
      else {
        return true;
      } 
      hits++;
    }
    
    //Button6 validation
    if (button6 == HIGH) {
      int ans = 6;
      if (solutions[hits] == ans) {
        digitalWrite((ans+nLeds), HIGH);
        delay(350);
        digitalWrite((ans+nLeds), LOW);
        delay(200);
      }
      else {
        return true;
      } 
      hits++;

    }
  }
  
  //Did the user fail?
  return false;
  
}
