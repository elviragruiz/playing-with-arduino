//Buttons value init
int restart = 0;
int button3 = 0;
int button4 = 0;

//Other Global vars
int nLeds = 2;

int MINLED = 5;
int MAXLED = 7;

//void simonSays();
//bool simonChallenge(int difficulty);

void setup() {
  // put your setup code here, to run once:
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  pinMode(4, INPUT);
  Serial.begin(9600);      // open the serial port at 9600 bps:    
}

void loop() {
  // put your main code here, to run repeatedly:
  restart = digitalRead(2);

  if (restart == HIGH) {
    //Start game
    simonSays();
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
    //CONTROL LED
    digitalWrite(7, LOW);
  } 
}


void simonSays() {
  int i = 0;
  bool failure = false;
  while (!failure) {
    i++;
    //Difficulty blink
    for(int j = 0; j < i; j++){
      digitalWrite(7, HIGH);
      delay(200);
      digitalWrite(7, LOW);
      delay(100);
    }
    failure = simonChallenge(i);
  }
  //Fin de la ejecución
  digitalWrite(5, HIGH);
  digitalWrite(6, HIGH);
  digitalWrite(7, HIGH);
  delay(50);
  digitalWrite(5, LOW);
  digitalWrite(6, LOW);
  digitalWrite(7, LOW);
  delay(250);
}

bool simonChallenge(int difficulty){
  int sequence[difficulty];
  int solutions[difficulty];
  int hits = 0;
  
  //The random sequence is created
  for (int i = 0; i < difficulty; i++) {

    //Random LED is chosen
    sequence[i] = random(5, 7);
    Serial.println(sequence[i]);
    //Blink of the new LED inserted
    digitalWrite(sequence[i], HIGH);
    delay(250);
    digitalWrite(sequence[i], LOW);
    delay(250);
    //Each LED is associated with its button    
    solutions[i] = sequence[i] - nLeds;
  }
  //Check user input
  while (hits < difficulty) {
    button3 = digitalRead(3);
    button4 = digitalRead(4);

    //Button3 validation
    if (button3 == HIGH) {
      if (solutions[hits] == 3) {
        digitalWrite((3+nLeds), HIGH);
        delay(600);
        digitalWrite((3+nLeds), LOW);
        delay(300);
      }
      else {
        return true;
      }
      hits++;

    }

    //Button4 validation
    if (button4 == HIGH) {
      if (solutions[hits] == 4) {
        digitalWrite((4+nLeds), HIGH);
        delay(600);
        digitalWrite((4+nLeds), LOW);
        delay(200);
      }
      else {
        return true;
      } 
      hits++;

    }
  }

  //Did the user fail?
  return false;

}
